package com.example.proj2

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.util.DiffUtil
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridLayout
import android.widget.ImageView
import android.widget.TextView
import kotlinx.android.synthetic.main.tracks.view.*
import kotlinx.android.synthetic.main.track_result_list.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.*
import kotlinx.android.synthetic.main.artist.*


@SuppressLint("ValidFragment")
class topArtistFragment(context: Context) : Fragment() {
    val con = context
    private var songList: ArrayList<song_Track> = ArrayList()
    private var adapter = trackAdapter(songList, con, 0)
    private var parentContext: Context? = this.context
    private lateinit var viewModel: SongViewModel
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    private var listener: OnFragmentInteractionListener? = null


    //    private var queryString: String = query
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    //fragment for the top artists streamed in the world
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.artist, container, false)
//        val recyclerView = view.findViewById(R.id.song_items_list) as RecyclerView
//        recyclerView.layoutManager = GridLayoutManager(context,2)
//        recyclerView.adapter = trackAdapter(songList)


////        recyclerView = view.findViewById(R.id.song_items_list);
////        recyclerView.setHasFixedSize(true);
////        var linearVertical = LinearLayoutManager(parentContext, LinearLayoutManager.VERTICAL, false)
////        recyclerView.setLayoutManager(linearVertical);
////        var linearHorizontal = LinearLayoutManager(parentContext, LinearLayoutManager.HORIZONTAL, false)
////        recyclerView.setLayoutManager(linearHorizontal);
////        var trackAdapter = trackAdapter(songList)
////        recyclerView.setAdapter(trackAdapter)
//
        return view
    }

//    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
//        val view = inflater.inflate(R.layout.track_result_list, container, false)
//        recyclerView = view.findViewById(R.id.song_items_list)
//        var layout: GridLayoutManager = GridLayoutManager(parentContext, 3)
//        recyclerView.setHasFixedSize(true)
//        recyclerView.setLayoutManager(layout)
//        var trackAdapter = trackAdapter(songList)
//        recyclerView.setAdapter(trackAdapter)
//        return view
//    }


    override fun onStart() {
        super.onStart()
//        val displayText = "Search for: $queryString"
//        (activity as AppCompatActivity).supportActionBar?.title = displayText
//        parentContext = context
        item.layoutManager = GridLayoutManager(this.context,1)
        item.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        viewModel = ViewModelProviders.of(this).get(SongViewModel::class.java)
//        song_items_list.setAdapter( adapter );


        val observer = Observer<ArrayList<song_Track>> {
            item.adapter = adapter
            val result = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun areItemsTheSame(p0: Int, p1: Int): Boolean {
                    return songList[p0].getsongname() == songList[p1].getsongname()
                }

                override fun getOldListSize(): Int {
                    return songList.size
                }

                override fun getNewListSize(): Int {
                    if (it == null) {
                        return 0
                    }
                    return it.size
                }

                override fun areContentsTheSame(p0: Int, p1: Int): Boolean {
                    return (songList[p0] == songList[p1])
                }
            })
            adapter = trackAdapter(it ?: ArrayList(), con, 0)
            item.adapter = adapter
            songList = it ?: ArrayList()
        }
        viewModel.getTopArtist().observe(this, observer)
//        song_items_list.setAdapter( adapter );
//        viewModel.SongAsyncTask().execute()

    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }


}










