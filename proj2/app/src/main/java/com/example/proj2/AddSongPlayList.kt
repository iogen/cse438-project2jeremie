package com.example.proj2

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.util.DiffUtil
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.track_result_list.*

@SuppressLint("ValidFragment")
class AddSongPlayList(context: Context) : Fragment() {
    val con = context
    private var songList: ArrayList<song_Track> = ArrayList()
    private var adapter = trackAdapter(songList, con, 1)
    private var parentContext: Context? = this.context
    private lateinit var viewModel: SongViewModel


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.playlist, container, false)

        return view
    }

//This is my playlist tab and stores and updates the playlist
    override fun onStart() {
        super.onStart()

        song_items_list.layoutManager = GridLayoutManager(this.context, 1)
        song_items_list.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        viewModel = ViewModelProviders.of(this).get(SongViewModel::class.java)


        val observer = Observer<ArrayList<song_Track>> {
            song_items_list.adapter = adapter
//            val result = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
//                override fun areItemsTheSame(p0: Int, p1: Int): Boolean {
//                    return songList[p0].getsongname() == songList[p1].getsongname()
//                }
//
//                override fun getOldListSize(): Int {
//                    return songList.size
//                }
//
//                override fun getNewListSize(): Int {
//                    if (it == null) {
//                        return 0
//                    }
//                    return it.size
//                }
//
//                override fun areContentsTheSame(p0: Int, p1: Int): Boolean {
//                    return (songList[p0] == songList[p1])
//                }
//            })

            adapter = trackAdapter(it ?: ArrayList(), con, 1)
            song_items_list.adapter = adapter
            songList = it ?: ArrayList()
        }
//        song_items_list.setAdapter( adapter );
//        viewModel.SongAsyncTask().execute()

        viewModel.getSongs().observe(this, observer)
    }
//Allos the observer to update everytime new data or data deleted


//    @SuppressLint("MissingSuperCall")
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//
//        ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
//            override fun onMove(
//                recyclerView: RecyclerView,
//                viewHolder: RecyclerView.ViewHolder,
//                viewHolder1: RecyclerView.ViewHolder
//            ): Boolean {
//                return false
//            }
//
//            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, i: Int) {
////                db.removeItem(viewHolder.itemId)
//                val db = AddSongDatabaseHelper(con)
//                db.removeItem(viewHolder.itemId)
////                db.deleteName(Name)
////            db.removeItem(Name)
//            }
//        }).attachToRecyclerView(song_items_list)
//
//    }


}