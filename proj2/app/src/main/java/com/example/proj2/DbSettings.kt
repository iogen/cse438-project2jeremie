package com.example.proj2

import android.provider.BaseColumns

//My database class
class DbSettings {
    companion object {
        const val DB_NAME = "playlist.db"
        const val DB_VERSION = 1
    }

    class DBPlaylistEntry: BaseColumns {
        companion object {
            const val TABLE = "playlist"
            const val ID = BaseColumns._ID
            const val COL_SONGNAME = "songname"
            const val COL_ARTISTNAME = "artistname"
            const val COL_IMAGE = "image"
            const val COL_LISTENERS = "listeners"
            const val COL_URL = "url"
        }
    }

}