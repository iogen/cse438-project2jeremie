package com.example.proj2

import android.R
import android.annotation.SuppressLint
import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.database.Cursor
import android.os.AsyncTask
import android.provider.BaseColumns
import android.util.Log
import android.widget.ArrayAdapter


class SongViewModel(application: Application) : AndroidViewModel(application) {
    private var _favouriteDBHelper: AddSongDatabaseHelper = AddSongDatabaseHelper(application)
    private var _songList: MutableLiveData<ArrayList<song_Track>> = MutableLiveData()
    var count = 0
    lateinit var word: String
    lateinit var box: String


//    fun getTop(): ArrayList<song_Track> {
//        return QueryUtils.fetchTopTracks()
//        // needs to make this an async call
//    }
//

    //gets top track in world
    fun getTopTracks(): MutableLiveData<ArrayList<song_Track>> {
        loadProducts("")
        count = 1
        var g = count
        return _songList
    }

    //gets top track from a specific artist
    fun getTopTracksArtist(query: String): MutableLiveData<ArrayList<song_Track>> {
        loadProducts(query)
        count = 2
        word = query
        return _songList
    }

    //gets top streamed artists
    fun getTopArtist(): MutableLiveData<ArrayList<song_Track>> {
        loadProducts("")
        count = 3
        var k = 5
        return _songList
    }

    //get the song
    fun getTheSong(query: String): MutableLiveData<ArrayList<song_Track>> {
        loadProducts(query)
        count = 4
        box = query
        return _songList
    }

    fun getProductsByQueryText(query: String): MutableLiveData<ArrayList<song_Track>> {
        loadProducts("?filter={\"where\":{\"name\":{\"like\":\".*$query.*\",\"options\":\"i\"}}}")
        return _songList
    }

    //loads data async
    private fun loadProducts(query: String) {
        SongAsyncTask().execute(query)
    }

    //depedning on which count is at, which is a gloabal variable it will do a specific async call
    @SuppressLint("StaticFieldLeak")
    inner class SongAsyncTask : AsyncTask<String, Unit, ArrayList<song_Track>>() {
        override fun doInBackground(vararg params: String?): ArrayList<song_Track>? {
//            return QueryUtils.fetchProductData(params[0]!!)
            if (count == 1) {
                return QueryUtil.fetchTopTracks()
            } else if (count == 2) {
                return QueryUtil.fetchArtistTopTracks(word)
            } else if (count == 4) {
                return QueryUtil.fetchSong(box)
            }
            return QueryUtil.fetchTopArtists()


        }

        override fun onPostExecute(result: ArrayList<song_Track>?) {
            if (result == null) {
                Log.e("RESULTS", "No Results Found")
            } else {
                //done
                Log.e("RESULTS", result.toString())
                val s: java.util.ArrayList<song_Track> = java.util.ArrayList<song_Track>()
                for (item in result) {
                    s.add(item)
                }

                _songList.value = s

//                var adapter = ArrayAdapter<String>(context, R.layout.simple_spinner_item,s)
//                this.act.categorySpinner.adapter = adapter
            }
        }
    }
//get the song list
    fun getSongs(): MutableLiveData<ArrayList<song_Track>> {
        loadSongs()
        return _songList
    }

    //loads the song
    private fun loadSongs(){
        val favorites: ArrayList<song_Track> = ArrayList()
        val database = this._favouriteDBHelper.readableDatabase


        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        val projection = arrayOf(
            BaseColumns._ID,
            DbSettings.DBPlaylistEntry.COL_SONGNAME,
            DbSettings.DBPlaylistEntry.COL_ARTISTNAME,
            DbSettings.DBPlaylistEntry.COL_IMAGE,
            DbSettings.DBPlaylistEntry.COL_LISTENERS,
            DbSettings.DBPlaylistEntry.COL_URL
        )


        val cursor = database.query(
            DbSettings.DBPlaylistEntry.TABLE,   // The table to query
            projection,             // The array of columns to return (pass null to get all)
            null,              // The columns for the WHERE clause
            null,          // The values for the WHERE clause
            null,                   // don't group the rows
            null,                   // don't filter by row groups
            null               // The sort order
        )

        var songs = ArrayList<song_Track>()
        with(cursor) {
            while (moveToNext()) {
                val song_name = getString(getColumnIndexOrThrow(DbSettings.DBPlaylistEntry.COL_SONGNAME))
                val artist_name = getString(getColumnIndexOrThrow(DbSettings.DBPlaylistEntry.COL_ARTISTNAME))
                val albumn_pic = getString(getColumnIndexOrThrow(DbSettings.DBPlaylistEntry.COL_IMAGE))
                val listeners = getString(getColumnIndexOrThrow(DbSettings.DBPlaylistEntry.COL_LISTENERS))
                val url = getString(getColumnIndexOrThrow(DbSettings.DBPlaylistEntry.COL_URL)) ?: ""
                val j = song_Track(song_name, artist_name, albumn_pic, listeners, url)
                songs.add(j)
            }
        }
        _songList.value = songs
    }

    //gets all items in songlist
    private fun getAllItems(): Cursor{
        val database = this._favouriteDBHelper.readableDatabase

        val projection = arrayOf(
            BaseColumns._ID,
            DbSettings.DBPlaylistEntry.COL_SONGNAME,
            DbSettings.DBPlaylistEntry.COL_ARTISTNAME,
            DbSettings.DBPlaylistEntry.COL_IMAGE,
            DbSettings.DBPlaylistEntry.COL_LISTENERS,
            DbSettings.DBPlaylistEntry.COL_URL
        )

        return database.query(
            DbSettings.DBPlaylistEntry.TABLE,   // The table to query
            projection,             // The array of columns to return (pass null to get all)
            null,              // The columns for the WHERE clause
            null,          // The values for the WHERE clause
            null,                   // don't group the rows
            null,                   // don't filter by row groups
            null               // The sort order
        )
    }




//
}