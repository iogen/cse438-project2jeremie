Creative

My app shows the top Artists in the world by streams from LastFM, while showing the name title which is their streams, the artist picture, listeners and url to find them for everything
My app can search by song name 

Both of these are done using api calls and completly different extract data from json methods

I created 4 tabs and it works efficiently without crashing on 4 tabs because it is easier for the user to click any tab they want and automatically be transported there

(10 / 10 points) The app displays the current top tracks in a GridView on startup\
	(-5) No Gridview
(10 / 10 points) The app uses a tab bar with two tabs, one for searching for tracks and one for looking at the playlist
(10 / 10 points) Data is pulled from the API and processed into a GridView on the main page. Makes use of a Fragment to display the results seamlessly.
(15 / 15 points) Selecting a track from the GridView opens a new activity with the track cover, title, and 3 other pieces of information as well as the ability to save it to the playlist.
(5 / 5 points) User can change search query by editing text field.
(10 / 10 points) User can save a track to their playlist, and the track is saved into a SQLite database.
(5 / 5 points) User can delete a track from the playlist (deleting it from the SQLite database itself).
(4 / 4 points) App is visually appealing
(1/ 1 point) Properly attribute Last.fm API as source of data.
(5 / 5 points) Code is well formatted and commented.
(10 / 10 points) All API calls are done asynchronously and do not stall the application.
(7 / 15 points) Creative portion: Be creative!
	The top artists page allows you to add an artist to a playlist, which is confusing. The app also crashed on me a few times after switching between artist and song searches.

Total: 87 / 100