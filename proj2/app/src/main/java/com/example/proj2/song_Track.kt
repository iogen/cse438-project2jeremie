package com.example.proj2

import java.io.Serializable

//song track serializable that i use to add songs in this format everywhere
class song_Track (): Serializable{

    private var songname: String = ""
    private var artistname: String = ""
    private var image: String = ""
    private var listeners: String = ""
    private var url: String = ""


    constructor(
        songname: String,
        artistname: String,
        image: String,
        listeners: String,
        url: String
    ) : this() {
        this.songname = songname
        this.artistname = artistname
        this.image = image
        this.listeners = listeners
        this.url = url
    }


    fun getsongname(): String{
        return this.songname
    }
    fun getartistname(): String{
        return this.artistname
    }
    fun getimage(): String{
        return this.image
    }
    fun getlisteners(): String{
        return this.listeners
    }fun geturl(): String{
        return this.url
    }


}
