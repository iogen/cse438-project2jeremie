package com.example.proj2

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.BaseColumns
import android.widget.Adapter
import android.widget.Toast
import com.example.proj2.DbSettings.DBPlaylistEntry.Companion.COL_ARTISTNAME
import com.example.proj2.DbSettings.DBPlaylistEntry.Companion.COL_SONGNAME
import com.example.proj2.DbSettings.DBPlaylistEntry.Companion.ID
import com.example.proj2.DbSettings.DBPlaylistEntry.Companion.TABLE

class AddSongDatabaseHelper(context: Context) :
    SQLiteOpenHelper(context, DbSettings.DB_NAME, null, DbSettings.DB_VERSION) {
    var con = context
    override fun onCreate(db: SQLiteDatabase?) {
        val createFavoritesTableQuery = "CREATE TABLE " + DbSettings.DBPlaylistEntry.TABLE + " ( " +
                DbSettings.DBPlaylistEntry.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DbSettings.DBPlaylistEntry.COL_SONGNAME + " TEXT NULL, " +
                DbSettings.DBPlaylistEntry.COL_ARTISTNAME + " TEXT NULL, " +
                DbSettings.DBPlaylistEntry.COL_IMAGE + " TEXT NULL, " +
                DbSettings.DBPlaylistEntry.COL_LISTENERS + " TEXT NULL, " +
                DbSettings.DBPlaylistEntry.COL_URL + " TEXT NULL)"


        db?.execSQL(createFavoritesTableQuery)
//        db?.execSQL(createImageAssetTableQuery)

    }


    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL("DROP TABLE IF EXISTS " + DbSettings.DBPlaylistEntry.TABLE)
        onCreate(db)
    }
    // This method adds the song to databas and only allows 1 song and no duplicates
    fun addSong(songname: String, artistname: String, image: String, listeners: String, url: String) {
        // Gets the data repository in write mode
        val db = this.writableDatabase


// Create a new map of values, where column names are the keys
        val values = ContentValues().apply {
            put(DbSettings.DBPlaylistEntry.COL_SONGNAME, songname)
            put(DbSettings.DBPlaylistEntry.COL_ARTISTNAME, artistname)
            put(DbSettings.DBPlaylistEntry.COL_IMAGE, image)
            put(DbSettings.DBPlaylistEntry.COL_LISTENERS, listeners)
            put(DbSettings.DBPlaylistEntry.COL_URL, url)

        }


// Checks if it is in database
        var check = searchTrack(db,songname, artistname)

        if(check == 1) {
            // Insert the new row, returning the primary key value of the new row
            val newRowId = db?.insert(DbSettings.DBPlaylistEntry.TABLE, null, values)
            Toast.makeText(con, "Song Added!", Toast.LENGTH_SHORT).show()

        }
        else{
            Toast.makeText(con, "Song Already in Playlist!", Toast.LENGTH_SHORT).show()
        }
}

//    fun getItemID(name: String): Int {
//        val db = this.writableDatabase
//        val query = "SELECT $ID FROM $TABLE WHERE $COL_SONGNAME = $name'"
//        return db.execSQL(query)
//    }

    //Method deletes a song from the databse
    fun deleteName(name: String) {
        val db = this.writableDatabase
        val query = ("DELETE FROM " + TABLE + " WHERE " + COL_SONGNAME + "= \"" + name + "\"")
        try {
            db.execSQL(query)
            Toast.makeText(con, "Song Deleted!", Toast.LENGTH_SHORT).show()
        } catch (query: Exception) {
            Toast.makeText(con, "Song not Found to Delete!", Toast.LENGTH_SHORT).show()

        }
    }
//same as deleteName function
    fun removeItem(name: String) {
        val db = this.writableDatabase
        try {
            db.delete(TABLE, "$COL_SONGNAME = $'name'", null)
            Toast.makeText(con, "Song Deleted!", Toast.LENGTH_SHORT).show()

        } catch (query: Exception) {
            Toast.makeText(con, "Song not Found to Delete!", Toast.LENGTH_SHORT).show()

        }
    }
//Search track is  helper method to make sure that no duplicates are added to the databse
    fun searchTrack(db: SQLiteDatabase?, name: String, artist: String): Int {

        val projection = arrayOf(
            BaseColumns._ID,
            DbSettings.DBPlaylistEntry.COL_SONGNAME,
            DbSettings.DBPlaylistEntry.COL_ARTISTNAME,
            DbSettings.DBPlaylistEntry.COL_IMAGE,
            DbSettings.DBPlaylistEntry.COL_LISTENERS,
            DbSettings.DBPlaylistEntry.COL_URL
        )


        val cursor = db!!.query(
            DbSettings.DBPlaylistEntry.TABLE,   // The table to query
            projection,             // The array of columns to return (pass null to get all)
            null,              // The columns for the WHERE clause
            null,          // The values for the WHERE clause
            null,                   // don't group the rows
            null,                   // don't filter by row groups
            null               // The sort order
        )

        while (cursor.moveToNext()) {

            var table_n = cursor.getColumnIndexOrThrow("songname")
            var Name = cursor.getString(table_n)

            var table_a = cursor.getColumnIndexOrThrow("artistname")
            var Artist = cursor.getString(table_a)

            var N = Name.equals(name)
            var A = Artist.equals(artist)
            if (N && A){
                return 0
            }
        }
        return 1
    }

}