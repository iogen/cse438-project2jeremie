package com.example.proj2

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.util.DiffUtil
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import kotlinx.android.synthetic.main.track_result_list.*
import kotlinx.android.synthetic.main.search.*


@SuppressLint("ValidFragment")
class searchFragment(context: Context) : Fragment() {
    val con = context
    private var songList: ArrayList<song_Track> = ArrayList()
    private var adapter = trackAdapter(songList, con, 0)
    private var parentContext: Context? = this.context
    private lateinit var viewModel: SongViewModel
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    private var listener: OnFragmentInteractionListener? = null


    //    private var queryString: String = query
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }
//Fragment for searching the for a song or artist

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.search, container, false)
//        val recyclerView = view.findViewById(R.id.song_items_list) as RecyclerView
//        recyclerView.layoutManager = GridLayoutManager(context,2)
//        recyclerView.adapter = trackAdapter(songList)


////        recyclerView = view.findViewById(R.id.song_items_list);
////        recyclerView.setHasFixedSize(true);
////        var linearVertical = LinearLayoutManager(parentContext, LinearLayoutManager.VERTICAL, false)
////        recyclerView.setLayoutManager(linearVertical);
////        var linearHorizontal = LinearLayoutManager(parentContext, LinearLayoutManager.HORIZONTAL, false)
////        recyclerView.setLayoutManager(linearHorizontal);
////        var trackAdapter = trackAdapter(songList)
////        recyclerView.setAdapter(trackAdapter)
//
        return view
    }

//    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
//        val view = inflater.inflate(R.layout.track_result_list, container, false)
//        recyclerView = view.findViewById(R.id.song_items_list)
//        var layout: GridLayoutManager = GridLayoutManager(parentContext, 3)
//        recyclerView.setHasFixedSize(true)
//        recyclerView.setLayoutManager(layout)
//        var trackAdapter = trackAdapter(songList)
//        recyclerView.setAdapter(trackAdapter)
//        return view
//    }


    override fun onStart() {
        super.onStart()
//        val displayText = "Search for: $queryString"
//        (activity as AppCompatActivity).supportActionBar?.title = displayText
//        parentContext = context
        RC.layoutManager = GridLayoutManager(this.context, 1)
        RC.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        viewModel = ViewModelProviders.of(this).get(SongViewModel::class.java)
//        song_items_list.setAdapter( adapter );


        val observer = Observer<ArrayList<song_Track>> {
            RC.adapter = adapter
            val result = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun areItemsTheSame(p0: Int, p1: Int): Boolean {
                    return songList[p0].getsongname() == songList[p1].getsongname()
                }

                override fun getOldListSize(): Int {
                    return songList.size
                }

                override fun getNewListSize(): Int {
                    if (it == null) {
                        return 0
                    }
                    return it.size
                }

                override fun areContentsTheSame(p0: Int, p1: Int): Boolean {
                    return (songList[p0] == songList[p1])
                }
            })
            adapter = trackAdapter(it ?: ArrayList(), con, 0)
            RC.adapter = adapter
            songList = it ?: ArrayList()
        }
//        viewModel.getTopTracksArtist().observe(this, observer)

        var box = view!!.findViewById<EditText>(R.id.search)
        var text = box.text.toString()
        Log.e("okay", text)

//searches for a song button
        val searchArtist: Button = view!!.findViewById(R.id.artistSearchBut)
        searchArtist.setOnClickListener {
            text = box.text.toString()
            viewModel.getTopTracksArtist(text).observe(this, observer)
        }
//searches for an artist button
        val searchSong: Button = view!!.findViewById(R.id.songSearchBut)
        searchSong.setOnClickListener {
            text = box.text.toString()
            viewModel.getTheSong(text).observe(this, observer)
        }


    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }


}










