package com.example.proj2

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import kotlinx.android.synthetic.main.tab.*
import android.content.Context



class TabLayout : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.first)

        val fragmentAdapter = MyPagerAdapter(supportFragmentManager, this)
        viewpager_main.adapter = fragmentAdapter


        tabs_main.setupWithViewPager(viewpager_main)
    }

//Makes the 4 tab layouts

    class MyPagerAdapter(fm: FragmentManager, context: Context) : FragmentPagerAdapter(fm) {
        val placeholder = context
        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> {
                    trackFragment(placeholder)
//                    searchFragment(placeholder)
//                  top 20 tracks fragment
                }
                1 -> {
                    searchFragment(placeholder)
//                    trackFragment(placeholder)
//                    AddSongPlayList()
//                  searching fragment
                }
                2 -> {
                    topArtistFragment(placeholder)
//                    AddSongPlayList()
//                  searching fragment
                }
                else -> AddSongPlayList(placeholder)
//                add song to playlist fragment
//                else -> JokeList()
            }
        }

        override fun getCount(): Int {
            return 4
        }

        override fun getPageTitle(position: Int): CharSequence {
            return when (position) {
                0 -> "Top Tracks"
                1 -> "Search"
                2 -> "Top Artists"
                else -> {
                    return "Playlist"
                }
            }
        }
    }
}
