package com.example.proj2

import android.text.TextUtils
import android.util.Log
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.nio.charset.Charset
import kotlin.collections.ArrayList

class QueryUtil {
    companion object {
        private val LogTag = this::class.java.simpleName
        private const val BaseURL = "http://ws.audioscrobbler.com/2.0/" // localhost URL

        // fetches top tracks
        fun fetchTopTracks(): ArrayList<song_Track> {
            val jsonQueryString =
                "?method=chart.gettoptracks&api_key=cc46292cd409d8e78c33d1e982748fe9&format=json"
            val url: URL? = createUrl("${this.BaseURL}$jsonQueryString")

            var jsonResponse: String? = null
            try {
                jsonResponse = makeHttpRequest(url)
            } catch (e: IOException) {
                Log.e(this.LogTag, "Problem making the HTTP request.", e)
            }

            return this.extractDataFromJsonTop(jsonResponse)!!
        }
//fetches an artist most played tracks
        fun fetchArtistTopTracks(artist: String): ArrayList<song_Track> {
//            val jsonQueryString =
//                "?method=artist.gettoptracks&artist=cher&api_key=cc46292cd409d8e78c33d1e982748fe9&format=json"
//            val jsonQueryString =
//                "?method=artist.gettoptracks&artist=" + (artist) + "&api_key=cc46292cd409d8e78c33d1e982748fe9&format=json"
            val beg = "?method=artist.gettoptracks&artist="
            val mid = artist
            val end = "&api_key=cc46292cd409d8e78c33d1e982748fe9&format=json"
            val jsonQueryString = beg + mid + end

            val url: URL? = createUrl("${this.BaseURL}$jsonQueryString")

            var jsonResponse: String? = null
            try {
                jsonResponse = makeHttpRequest(url)
            } catch (e: IOException) {
                Log.e(this.LogTag, "Problem making the HTTP request.", e)
            }

            return this.extractDataFromJson(jsonResponse)!!
        }
//fetches the most streamed artists
        fun fetchTopArtists(): ArrayList<song_Track> {
//            val jsonQueryString =
//                "?method=artist.gettoptracks&artist=cher&api_key=cc46292cd409d8e78c33d1e982748fe9&format=json"
            val jsonQueryString =
                "?method=chart.gettopartists&api_key=cc46292cd409d8e78c33d1e982748fe9&format=json"

            val url: URL? = createUrl("${this.BaseURL}$jsonQueryString")

            var jsonResponse: String? = null
            try {
                jsonResponse = makeHttpRequest(url)
            } catch (e: IOException) {
                Log.e(this.LogTag, "Problem making the HTTP request.", e)
            }

            return this.extractDataFromJsonArtist(jsonResponse)!!
        }

//fetches a specific song
        fun fetchSong(s: String): ArrayList<song_Track> {
//            val jsonQueryString =
//                "?method=artist.gettoptracks&artist=cher&api_key=cc46292cd409d8e78c33d1e982748fe9&format=json"
            val jsonQueryString =
                "?method=track.search&track=" + (s) + "&api_key=cc46292cd409d8e78c33d1e982748fe9&format=json"
            val url: URL? = createUrl("${this.BaseURL}$jsonQueryString")

            var jsonResponse: String? = null
            try {
                jsonResponse = makeHttpRequest(url)
            } catch (e: IOException) {
                Log.e(this.LogTag, "Problem making the HTTP request.", e)
            }

            return this.extractDataFromJsonS(jsonResponse)!!
        }


        private fun createUrl(stringUrl: String): URL? {
            var url: URL? = null
            try {
                url = URL(stringUrl)
            } catch (e: MalformedURLException) {
                Log.e(this.LogTag, "Problem building the URL.", e)
            }

            return url
        }

        private fun makeHttpRequest(url: URL?): String {
            var jsonResponse = ""

            if (url == null) {
                return jsonResponse
            }

            var urlConnection: HttpURLConnection? = null
            var inputStream: InputStream? = null
            try {
                urlConnection = url.openConnection() as HttpURLConnection
                urlConnection.readTimeout = 10000 // 10 seconds
                urlConnection.connectTimeout = 15000 // 15 seconds
                urlConnection.requestMethod = "GET"
                urlConnection.connect()

                if (urlConnection.responseCode == 200) {
                    inputStream = urlConnection.inputStream
                    jsonResponse = readFromStream(inputStream)
                } else {
                    Log.e(this.LogTag, "Error response code: ${urlConnection.responseCode}")
                }
            } catch (e: IOException) {
                Log.e(this.LogTag, "Problem retrieving the product data results: $url", e)
            } finally {
                urlConnection?.disconnect()
                inputStream?.close()
            }

            return jsonResponse
        }

        private fun readFromStream(inputStream: InputStream?): String {
            val output = StringBuilder()
            if (inputStream != null) {
                val inputStreamReader = InputStreamReader(inputStream, Charset.forName("UTF-8"))
                val reader = BufferedReader(inputStreamReader)
                var line = reader.readLine()
                while (line != null) {
                    output.append(line)
                    line = reader.readLine()
                }
            }

            return output.toString()
        }
//this gets the tracks from the top in world and extracts json
        private fun extractDataFromJsonTop(songJson: String?): ArrayList<song_Track>? {
            if (TextUtils.isEmpty(songJson)) {
                return null
            }

            val songList = ArrayList<song_Track>()


            try {
                val baseJsonResponse = JSONObject(songJson)

                val toptracks = baseJsonResponse.getJSONObject("tracks")
                val track = toptracks.getJSONArray("track")


                for (i in 0 until track.length()) {
                    val songObject = track.getJSONObject(i)

                    // Image
                    val image = returnValueOrDefault<JSONArray>(songObject, "image") as JSONArray
                    val imageArrayList = ArrayList<String>()
                    var imageS = ""

                    for (j in 0 until image.length()) {
                        val imageOBJ = image.getJSONObject(j)
                        val testImage = imageOBJ.getString("size")
                        if (testImage == "medium") {
                            imageArrayList.add(imageOBJ.getString("#text"))
                            imageS = imageOBJ.getString("#text")
                        }
                    }


                    // artist
                    val artist = returnValueOrDefault<JSONObject>(songObject, "artist") as JSONObject

                    val songname = returnValueOrDefault<String>(songObject, "name") as String
                    val artistname = returnValueOrDefault<String>(artist, "name") as String

                    val listeners = returnValueOrDefault<String>(songObject, "listeners") as String
                    val url = returnValueOrDefault<String>(songObject, "url") as String



                    songList.add(
                        song_Track(
                            songname,
                            artistname,
                            imageS,
                            listeners,
                            url
                        )
                    )
                    var k = 5
                }
                var s = 6
            } catch (e: JSONException) {
                Log.e(this.LogTag, "Problem parsing the product JSON results", e)
            }

            return songList
        }
//this gets the song and extracts json

        private fun extractDataFromJsonS(songJson: String?): ArrayList<song_Track>? {
            if (TextUtils.isEmpty(songJson)) {
                return null
            }

            val songList = ArrayList<song_Track>()


            try {
                val baseJsonResponse = JSONObject(songJson)

                val toptracks = baseJsonResponse.getJSONObject("results")
                val v = toptracks.getJSONObject("trackmatches")
                val track = v.getJSONArray("track")


                for (i in 0 until track.length()) {
                    val songObject = track.getJSONObject(i)

                    // Image
                    val image = returnValueOrDefault<JSONArray>(songObject, "image") as JSONArray
                    val imageArrayList = ArrayList<String>()
                    var imageS = ""

                    for (j in 0 until image.length()) {
                        val imageOBJ = image.getJSONObject(j)
                        val testImage = imageOBJ.getString("size")
                        if (testImage == "medium") {
                            imageArrayList.add(imageOBJ.getString("#text"))
                            imageS = imageOBJ.getString("#text")
                        }
                    }


                    val songname = returnValueOrDefault<String>(songObject, "name") as String
                    val artistname = returnValueOrDefault<String>(songObject, "name") as String


                    val listeners = returnValueOrDefault<String>(songObject, "listeners") as String
                    val url = returnValueOrDefault<String>(songObject, "url") as String



                    songList.add(
                        song_Track(
                            songname,
                            artistname,
                            imageS,
                            listeners,
                            url
                        )
                    )
                    var k = 5
                }
                var s = 6
            } catch (e: JSONException) {
                Log.e(this.LogTag, "Problem parsing the product JSON results", e)
            }

            return songList
        }
//this gets the artist from the top streamed in world and extracts json

        private fun extractDataFromJsonArtist(songJson: String?): ArrayList<song_Track>? {
            if (TextUtils.isEmpty(songJson)) {
                return null
            }

            val songList = ArrayList<song_Track>()


            try {
                val baseJsonResponse = JSONObject(songJson)

                val toptracks = baseJsonResponse.getJSONObject("artists")
                val track = toptracks.getJSONArray("artist")


                for (i in 0 until track.length()) {
                    val songObject = track.getJSONObject(i)

                    // Image
                    val image = returnValueOrDefault<JSONArray>(songObject, "image") as JSONArray
                    val imageArrayList = ArrayList<String>()
                    var imageS = ""

                    for (j in 0 until image.length()) {
                        val imageOBJ = image.getJSONObject(j)
                        val testImage = imageOBJ.getString("size")
                        if (testImage == "medium") {
                            imageArrayList.add(imageOBJ.getString("#text"))
                            imageS = imageOBJ.getString("#text")
                        }
                    }



                    val songname = returnValueOrDefault<String>(songObject, "playcount") as String
                    val artistname = returnValueOrDefault<String>(songObject, "name") as String


                    val listeners = returnValueOrDefault<String>(songObject, "listeners") as String
                    val url = returnValueOrDefault<String>(songObject, "url") as String



                    songList.add(
                        song_Track(
                            songname,
                            artistname,
                            imageS,
                            listeners,
                            url
                        )
                    )
                    var k = 5
                }
                var s = 6
            } catch (e: JSONException) {
                Log.e(this.LogTag, "Problem parsing the product JSON results", e)
            }

            return songList
        }
//this gets the top tracks from an artist extracts json

        private fun extractDataFromJson(songJson: String?): ArrayList<song_Track>? {
            if (TextUtils.isEmpty(songJson)) {
                return null
            }

            val songList = ArrayList<song_Track>()


            try {
                val baseJsonResponse = JSONObject(songJson)

                val toptracks = baseJsonResponse.getJSONObject("toptracks")
                val track = toptracks.getJSONArray("track")


                for (i in 0 until track.length()) {
                    val songObject = track.getJSONObject(i)

                    // Image
                    val image = returnValueOrDefault<JSONArray>(songObject, "image") as JSONArray
                    val imageArrayList = ArrayList<String>()
                    var imageS = ""

                    for (j in 0 until image.length()) {
                        val imageOBJ = image.getJSONObject(j)
                        val testImage = imageOBJ.getString("size")
                        if (testImage == "medium") {
                            imageArrayList.add(imageOBJ.getString("#text"))
                            imageS = imageOBJ.getString("#text")
                        }
                    }


                    // artist
                    val artist = returnValueOrDefault<JSONObject>(songObject, "artist") as JSONObject

                    val songname = returnValueOrDefault<String>(songObject, "name") as String
                    val artistname = returnValueOrDefault<String>(artist, "name") as String


                    val listeners = returnValueOrDefault<String>(songObject, "listeners") as String
                    val url = returnValueOrDefault<String>(songObject, "url") as String



                    songList.add(
                        song_Track(
                            songname,
                            artistname,
                            imageS,
                            listeners,
                            url
                        )
                    )
                    var k = 5
                }
                var s = 6
            } catch (e: JSONException) {
                Log.e(this.LogTag, "Problem parsing the product JSON results", e)
            }

            return songList
        }

        private inline fun <reified T> returnValueOrDefault(json: JSONObject, key: String): Any? {
            when (T::class) {
                String::class -> {
                    return if (json.has(key)) {
                        json.getString(key)
                    } else {
                        ""
                    }
                }
                Int::class -> {
                    return if (json.has(key)) {
                        json.getInt(key)
                    } else {
                        return -1
                    }
                }
                Double::class -> {
                    return if (json.has(key)) {
                        json.getDouble(key)
                    } else {
                        return -1.0
                    }
                }
                Long::class -> {
                    return if (json.has(key)) {
                        json.getLong(key)
                    } else {
                        return (-1).toLong()
                    }
                }
                JSONObject::class -> {
                    return if (json.has(key)) {
                        json.getJSONObject(key)
                    } else {
                        return null
                    }
                }
                JSONArray::class -> {
                    return if (json.has(key)) {
                        json.getJSONArray(key)
                    } else {
                        return null
                    }
                }
                else -> {
                    return null
                }
            }
        }
    }
}