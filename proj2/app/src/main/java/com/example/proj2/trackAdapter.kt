package com.example.proj2

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.Toast

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.tracks.view.*
import android.support.v4.content.ContextCompat.startActivity




//My main adpater that i use for everything, depding on if i pass 0 or 1  (type) it will go to add page or delete page


//    inner class trackAdapter(context: Context, tracks: List<Tracks>) :
class trackAdapter(var songList: ArrayList<song_Track>, context: Context, type: Int) :
    RecyclerView.Adapter<trackAdapter.TrackViewHolder>() {
    var con = context
    var TYPE = type

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): TrackViewHolder {
        val itemView = LayoutInflater.from(p0.context).inflate(R.layout.tracks, p0, false)
        return TrackViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return songList.size
    }


    override fun onBindViewHolder(p0: TrackViewHolder, p1: Int) {
        var NUM = TYPE
        val con = con
        val music = songList[p1]
        val musictitle = music.getsongname()
        val musicartist = music.getartistname()
        val musicpic = music.getimage()
        val listeners = music.getlisteners()
        val url = music.geturl()

        p0.title.text = musictitle
        p0.desc.text = musicartist

        Picasso.with(con).load(musicpic).into(p0.picture)

        val musicrow = p0.row

//        lateinit var savedInstanceState: Bundle

        if(NUM==0){
            p0.row.setOnClickListener { it: View? ->
                val intent = Intent(this@trackAdapter.con, Out::class.java)
                intent.putExtra("Song", music)
                intent.putExtra("image", musicpic)
                intent.putExtra("name", musictitle)
                intent.putExtra("artist", musicartist)
                intent.putExtra("listeners", listeners)
                intent.putExtra("url", url)
//            startActivity(con,intent, savedInstanceState)
                con.startActivity(intent)
            }
        }
        else{
            p0.row.setOnClickListener { it: View? ->
                val intent = Intent(this@trackAdapter.con, DELETE::class.java)
                intent.putExtra("Song", music)
                intent.putExtra("image", musicpic)
                intent.putExtra("name", musictitle)
                intent.putExtra("artist", musicartist)
                intent.putExtra("listeners", listeners)
                intent.putExtra("url", url)
//            startActivity(con,intent, savedInstanceState)
                con.startActivity(intent)
            }
        }

    }
//

    inner class TrackViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var row = itemView

        var picture: ImageView = itemView.img
        var title: TextView = itemView.title
        var desc: TextView = itemView.desc

    }
}