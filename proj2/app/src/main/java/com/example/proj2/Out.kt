package com.example.proj2

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.Toast
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.out.*


class Out : AppCompatActivity() {

//This is for adding the song to the playlist
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.out)

        var intent = intent

        var music = intent.getSerializableExtra("Song")
        var k = 5

//        val musictitle = music.getsongname()
//        val musicartist = music.getartistname()
//        val musicpic = music.getimage()
//        val musiclist = music.getlisteners()
//        val musicurl = music.geturl()

        val Image = intent.getStringExtra("image")
        val Name = intent.getStringExtra("name")
        val Artist = intent.getStringExtra("artist")
        val Listeners = intent.getStringExtra("listeners")
        val Url = intent.getStringExtra("url")

        Picasso.with(this).load(Image).into(image)
        name.text = "Title: " + Name
        artist.text = "Artist: " + Artist
        listeners.text = "Amount of listerners: " + Listeners
        url.text = "URL: " + Url


        val add: Button = findViewById(R.id.add)
        add.setOnClickListener {

            val db = AddSongDatabaseHelper(this)
            db.addSong(Name.toString(), Artist.toString(), Image.toString(), Listeners.toString(), Url.toString())
//            Adds song to playlist

        }

    }

}




